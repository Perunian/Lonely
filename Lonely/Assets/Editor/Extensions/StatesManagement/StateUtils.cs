using Engine.Core;
using Spark.Editor.Utils;
using Spark.Utils;
using System;
using System.IO;
using System.Text;
using UnityEditor;
using UnityEngine;

public static class StateUtils
{
    /// <summary>
    /// Creates localization fpr the script
    /// </summary>
    /// <param name="statesScriptsFolderPath">Folder of the States scripts</param>
    /// <param name="systemNamespace">Namespace of the states scripts</param>
    /// <param name="stateName">Name of the target State/Substate</param>
    /// <returns>String containing full path with of the script with the script name and extension </returns>
    public static string CreateScriptFullPath(string statesScriptsFolderPath, string systemNamespace, string stateName)
    {
        if (Application.platform == RuntimePlatform.OSXEditor)
        {
            return string.Join(null, statesScriptsFolderPath, "\\", systemNamespace.Replace('.', '\\'), "\\", stateName, ".cs").SetSlashes();
        }

        return string.Join(null, statesScriptsFolderPath, systemNamespace.Replace('.', '\\'), "\\", stateName, ".cs").SetSlashes();
    }
    /// <summary>
    /// Starts routine to create the script containing State/Substate
    /// </summary>
    /// <param name="stateType">Stat/Substate type</param>
    /// <param name="stateName">Target State/Substate name</param>
    /// <param name="namespaceId">Namespace of the script content</param>
    /// <param name="folderPath">Target folder for the script</param>
    /// <returns>MonoScript instance with the created State/Substate implementation</returns>
    public static MonoScript CreateNewState(StateNodeType stateType, string stateName, string namespaceId, string folderPath)
    {
        string text = stateName + ".cs";
        MonoScript result = null;
        if (!Directory.Exists(folderPath))
        {
            Directory.CreateDirectory(folderPath);
        }

        if (Directory.Exists(folderPath))
        {
            string newInputSystemScriptText = GetNewStateScriptText(stateType, Path.GetFileNameWithoutExtension(text), namespaceId);
            string pathToStateScript = Path.Combine(folderPath, text);
            if (File.Exists(pathToStateScript))
            {
                File.Delete(pathToStateScript);
            }

            File.WriteAllText(pathToStateScript, newInputSystemScriptText);
            AssetDatabase.Refresh(ImportAssetOptions.ForceSynchronousImport);
            result = AssetDatabase.LoadAssetAtPath<MonoScript>(AssetUtils.AbsoluteToRelativePath(pathToStateScript, PathReferenceRoot.Project, includeExtension: true, SlashPolicy.Auto));
        }

        return result;
    }

    /// <summary>
    /// Constructs default implementation of the State/Substate based on the configuration
    /// </summary>
    /// <param name="arch">State/Substate type</param>
    /// <param name="stateName">Name of the target State/Substate</param>
    /// <param name="systemNamespace">Namespace for the script content</param>
    /// <param name="includeOnAwake">Include OnAwake override?</param>
    /// <returns>Full content of the State/Substate implementation</returns>
    public static string GetNewStateScriptText(StateNodeType arch, string stateName, string systemNamespace, bool includeOnAwake = true)
    {
        StringBuilder sb = new StringBuilder("using Spark.Framework.Components;");
        sb.AppendLine();
        sb.AppendLine("using Engine.Core;");
        sb.AppendLine("using System.Collections;");
        sb.AppendLine("using UnityEngine;");
        sb.AppendLine("using UnityEngine.SceneManagement;");
        sb.AppendLine();
        sb.Append("namespace ");
        sb.AppendLine(systemNamespace); //by default it should be Gameplay.Flow
        sb.AppendLine("{");
        sb.Append("\tpublic class ");

        sb.Append(stateName);
        if (arch == StateNodeType.State)
        {
            sb.AppendLine(" : AppFlowState");
        }
        else
        {
            sb.AppendLine(" : AppFlowSubstate");
        }
        sb.AppendLine("\t{");

        if (includeOnAwake)
        {
            sb.AppendLine("\t\tpublic override void OnAwake()");
            sb.AppendLine("\t\t{");
            sb.AppendLine();
            sb.AppendLine("\t\t}");
            sb.AppendLine();
        }

        sb.AppendLine("\t\tpublic override bool CanEnter()");
        sb.AppendLine("\t\t{");
        sb.AppendLine("\t\t\treturn true;");
        sb.AppendLine("\t\t}");


        sb.AppendLine("\t\tpublic override IEnumerator Enter()");
        sb.AppendLine("\t\t{");
        sb.AppendLine("\t\t\tGetEvent<AppSubstateName>(AppStateControlEvent.SetSubstate).Report(AppSubstateName.Debug);");
        sb.AppendLine();
        sb.AppendLine("\t\t\tAsyncOperation loadingthread = SceneManager.LoadSceneAsync(sceneAssetReference.sceneIndex, LoadSceneMode.Additive);");
        sb.AppendLine();
        sb.AppendLine("\t\t\twhile (!loadingthread.isDone)");
        sb.AppendLine("\t\t\t{");
        sb.AppendLine("\t\t\t\tenterComplete = false;");
        sb.AppendLine("\t\t\t\tyield return null;");
        sb.AppendLine("\t\t\t}");
        sb.AppendLine();
        sb.AppendLine("\t\t\tenterComplete = true;");
        sb.AppendLine("\t\t}");

        sb.AppendLine("\t\tpublic override bool CanExit()");
        sb.AppendLine("\t\t{");
        sb.AppendLine("\t\t\treturn true;");
        sb.AppendLine("\t\t}");

        sb.AppendLine("\t\tpublic override IEnumerator Exit()");
        sb.AppendLine("\t\t{");
        sb.AppendLine("\t\t\tAsyncOperation unloadingThread = SceneManager.UnloadSceneAsync(sceneAssetReference.sceneIndex);");
        sb.AppendLine();
        sb.AppendLine("\t\t\twhile (!unloadingThread.isDone)");
        sb.AppendLine("\t\t\t{");
        sb.AppendLine("\t\t\t\texitComplete = false;");
        sb.AppendLine("\t\t\t\tyield return null;");
        sb.AppendLine("\t\t\t}");
        sb.AppendLine();
        sb.AppendLine("\t\t\texitComplete = true;");
        sb.AppendLine("\t\t}");

        sb.AppendLine("\t\tpublic override void Idle()");
        sb.AppendLine("\t\t{");
        sb.AppendLine();
        sb.AppendLine("\t\t}");

        sb.AppendLine("\t}");
        sb.AppendLine("}");

        return sb.ToString();
    }

    /// <summary>
    /// Finds and returns the prefab of AppFlowStateControlSystem based on MonoScript with the system script
    /// </summary>
    /// <param name="script">MonoScript with the AppFlowStateControlSystem implementation</param>
    /// <returns>Prefab with the system attached</returns>
    public static AppFlowStateControlSystem FindAppFlowSystemPrefab(MonoScript script)
    {
        if (script == null)
        {
            Debug.LogError("Please select a script to find.");
            return null;
        }

        string scriptPath = AssetDatabase.GetAssetPath(script);
        string[] guids = AssetDatabase.FindAssets("t:Prefab");

        foreach (string guid in guids)
        {
            string prefabPath = AssetDatabase.GUIDToAssetPath(guid);
            GameObject prefab = AssetDatabase.LoadAssetAtPath<GameObject>(prefabPath);

            if (prefab != null)
            {
                // Check if the prefab has the specified script attached
                MonoBehaviour[] components = prefab.GetComponentsInChildren<MonoBehaviour>(true);
                if (Array.Exists(components, component => component.GetType().ToString().Equals(script.GetClass().ToString(), StringComparison.OrdinalIgnoreCase)))
                {
                    //Debug.Log("Prefab with script found: " + prefabPath);

                    AppFlowStateControlSystem stateMachinePrefab = AssetDatabase.LoadAssetAtPath(prefabPath, typeof(AppFlowStateControlSystem)) as AppFlowStateControlSystem;

                    return stateMachinePrefab;
                }
            }
        }

        return null;
    }
}
