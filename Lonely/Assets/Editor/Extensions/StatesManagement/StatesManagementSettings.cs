using Spark.Framework.Components;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Extensions.StatesManagement
{
    /// <summary>
    /// Settings for StatesManagementExtension that store current requested states and substates by names
    /// </summary>
    public class StatesManagementSettings : SettingsComponent
    {
        public List<string> requestedStates = new List<string>();
        public List<string> requestedSubstates = new List<string>();
    }
}
