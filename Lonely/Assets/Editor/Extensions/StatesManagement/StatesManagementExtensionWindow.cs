using Engine.Core;
using Spark.Editor;
using Spark.Editor.Utils;
using Spark.Utils;
using System;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace Extensions.StatesManagement
{
    /// <summary>
    /// Window that serves as a View for the StateManagementExtension logic, displays the available 
    /// (if set up accoring to the architecture of this module) substate/state and allows to use the 
    /// logic of the extension based on the config from the window GUI
    /// </summary>
    public class StatesManagementExtensionWindow : SparkEditorWindow
	{
		private static StatesManagementExtensionWindow window;

        private AppFlowStateControlSystem appFlowSystemPrefab;

        private Vector2 statesScrollPosition;

        private GUIStyle errorTipStyle;
        private GUIStyle infoTipStyle;

        private string stateName = "ExampleNewStateName";

        private static string systemNamespaceId = "Gameplay.Flow";

        private int newStateType;
        private string[] stateTypes = null;

        public static bool IsActive
		{
			get { return window != null; }
		}

        /// <summary>
        /// Initialization of the Window
        /// </summary>
        /// <param name="sparkEditorSettings">SparkEditorSettings of the current project</param>
        /// <returns>StatesManagementExtensionWindow instance</returns>
		public static StatesManagementExtensionWindow Init(SparkEditorSettings sparkEditorSettings)
		{
			if (!IsActive)
			{
				SparkEditorSettings newSparkEditorSettings = null;

				if (sparkEditorSettings != null)
				{
					newSparkEditorSettings = sparkEditorSettings;
				}
				else
				{
					UnityEngine.Debug.LogWarning("No spark editor settings detected in " + window.name);
				}

				SetupWindow(newSparkEditorSettings);
			}

			return window;
		}

        /// <summary>
        /// Window setup based on the available States/Substates
        /// </summary>
        /// <param name="sparkEditorSettings">SparkEditorSettings instance of the current project</param>
		private static void SetupWindow(SparkEditorSettings sparkEditorSettings)
		{
			window = GetWindow<StatesManagementExtensionWindow>();
			window.sparkEditorSettings = sparkEditorSettings;
			window.SetupDimensions();
			window.name = "StatesManagementExtension";
			window.titleContent = new GUIContent(window.name);
			window.autoRepaintOnSceneChange = true;

            string[] types = Enum.GetNames(typeof(StateNodeType));
            window.stateTypes = types;

            window.errorTipStyle = new GUIStyle();
            window.errorTipStyle.fontSize = 8;
            window.errorTipStyle.normal.textColor = new Color(0.8f, 0, 0);

            window.infoTipStyle = new GUIStyle();
            window.infoTipStyle.fontSize = 8;
            window.infoTipStyle.normal.textColor = new Color(0, 0.6f, 0);

            MonoScript appFlowScript = GetSystemScript(typeof(AppFlowStateControlSystem));
            window.appFlowSystemPrefab = StateUtils.FindAppFlowSystemPrefab(appFlowScript);

            window.ShowPopupCenteredOnMainWindow(window.windowWidth, window.windowHeight);
		}

        /// <summary>
        /// Setup of the default dimensions
        /// </summary>
		private void SetupDimensions()
		{
			window.borderPadding = 5;
			windowWidth = 800;
			windowHeight = 600;
		}

        /// <summary>
        /// Styles setup
        /// </summary>
		private void SetupStyles()
		{
			//any setup for styling of the window elements

			stylesReady = true;
		}

        /// <summary>
        /// Routine to create the GUI of the Window
        /// </summary>
        private void OnGUI()
        {
            if (!stylesReady)
            {
                SetupStyles();
            }

            Handles.color = new Color(0.4f, 0.4f, 0.4f);

            int outerBorder = 1;
            Handles.DrawSolidRectangleWithOutline(new Rect(outerBorder, outerBorder, windowWidth - outerBorder * 1.5f, windowHeight - outerBorder * 1.5f), new Color(0, 0, 0, 0), Handles.color);
            borderPadding = 4;
            Handles.DrawSolidRectangleWithOutline(new Rect(borderPadding, borderPadding, windowWidth - borderPadding * 1.75f, windowHeight - borderPadding * 1.75f), new Color(0, 0, 0, 0), Handles.color);

            GUILayout.BeginArea(new Rect(borderPadding + windowWidth / 2.0f - 90.0f, windowHeight * 0.01f, 180, 20));
            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            GUILayout.Label("Create New State");
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();
            GUILayout.EndArea();

            GUILayout.BeginArea(new Rect(2 * contentPadding, windowHeight * 0.05f, windowWidth - 4 * contentPadding, 40));
            GUILayout.BeginHorizontal();
            GUILayout.Label("AppFlow prefab: ");
            GUI.enabled = false;
            EditorGUILayout.ObjectField("", appFlowSystemPrefab, typeof(AppFlowStateControlSystem), false, GUILayout.Width(650));
            GUI.enabled = true;
            GUILayout.EndHorizontal();
            GUILayout.EndArea();

            if (appFlowSystemPrefab != null)
            {
                GUILayout.BeginArea(new Rect(2 * contentPadding, windowHeight * 0.38f, windowWidth - 4 * contentPadding, windowHeight - (windowHeight * 0.33f) - 4 * contentPadding - 30));
                statesScrollPosition = GUILayout.BeginScrollView(statesScrollPosition, false, true);

                GUILayout.BeginVertical();
                GUIStyle labelStyle = GUI.skin.label;

                AppFlowState[] states = appFlowSystemPrefab.transform.GetComponentsInChildren<AppFlowState>();

                float deleteButtonWidth = windowWidth * 0.2f;

                for (int i = 0; i < states.Length; i++)
                {
                    GUILayout.BeginHorizontal();
                    string stateNameLabel = states[i].name;
                    float eventLabelSize = labelStyle.CalcSize(new GUIContent(stateNameLabel)).x;
                    if (eventLabelSize > 255)
                    {
                        stateNameLabel = stateNameLabel.Substring(0, 240) + "...";
                    }
                    
                    if (GUILayout.Button(stateNameLabel, 
                                         GUILayout.Width(windowWidth - contentPadding * 6 - deleteButtonWidth - 2f), 
                                         GUILayout.MaxWidth(windowWidth - contentPadding * 6 - deleteButtonWidth - 2f)))
                    {
                        AssetDatabase.OpenAsset(AssetDatabase.LoadAssetAtPath(AssetDatabase.GetAssetPath(appFlowSystemPrefab), typeof(AppFlowStateControlSystem)));
                        EditorGUIUtility.PingObject(states[i]);
                    }
                    if (GUILayout.Button("Delete", GUILayout.Width(deleteButtonWidth)))
                    {
                        GetEvent<StateNodeType, string>(StateManagementEvent.DeleteTargetState).Report((StateNodeType)newStateType, states[i].name);
                    }
                    GUILayout.EndHorizontal();
                }

                AppFlowSubstate[] subStates = appFlowSystemPrefab.transform.GetComponentsInChildren<AppFlowSubstate>();

                for (int i = 0; i < subStates.Length; i++)
                {
                    GUILayout.BeginHorizontal();
                    string stateNameLabel = subStates[i].name;
                    float eventLabelSize = labelStyle.CalcSize(new GUIContent(stateNameLabel)).x;
                    if (eventLabelSize > 255)
                    {
                        stateNameLabel = stateNameLabel.Substring(0, 240) + "...";
                    }
                    if (GUILayout.Button(stateNameLabel,
                                         GUILayout.Width(windowWidth - contentPadding * 6 - deleteButtonWidth - 2f),
                                         GUILayout.MaxWidth(windowWidth - contentPadding * 6 - deleteButtonWidth - 2f)))
                    {
                        AssetDatabase.OpenAsset(AssetDatabase.LoadAssetAtPath(AssetDatabase.GetAssetPath(appFlowSystemPrefab), typeof(AppFlowStateControlSystem)));
                        EditorGUIUtility.PingObject(subStates[i]);
                    }

                    if (GUILayout.Button("Delete", GUILayout.Width(deleteButtonWidth)))
                    {
                        GetEvent<StateNodeType, string>(StateManagementEvent.DeleteTargetState).Report((StateNodeType)newStateType, subStates[i].name);
                    }

                    GUILayout.EndHorizontal();
                }

                GUILayout.EndVertical();

                GUILayout.EndScrollView();
                GUILayout.EndArea();
            }

            GUILayout.BeginArea(new Rect(2 * contentPadding, windowHeight * 0.1f, windowWidth - 4 * contentPadding, 20));
            GUILayout.BeginVertical();
            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            GUILayout.Label("States path:");
            GUILayout.FlexibleSpace();
            GUI.enabled = false;
            EditorGUILayout.TextField(AssetDatabase.GetAssetPath(appFlowSystemPrefab), GUILayout.Width(725));
            GUI.enabled = true;
            GUILayout.EndHorizontal();
            GUILayout.EndVertical();
            GUILayout.EndArea();

            GUILayout.BeginArea(new Rect(2 * contentPadding, windowHeight * 0.15f, windowWidth - 4 * contentPadding, 40));
            GUILayout.BeginVertical();
            GUILayout.BeginHorizontal();
            GUILayout.Label("AppFlow state namespace");
            GUILayout.FlexibleSpace();
            GUI.enabled = false;
            EditorGUILayout.TextField(typeof(AppFlowStateControlSystem).Namespace, GUILayout.Width(725));
            GUI.enabled = true;
            GUILayout.EndHorizontal();
            GUILayout.EndVertical();
            GUILayout.EndArea();

            GUILayout.BeginArea(new Rect(2 * contentPadding, windowHeight * 0.2f, windowWidth - 4 * contentPadding, 40));
            GUILayout.BeginVertical();
            GUILayout.BeginHorizontal();
            newStateType = GUILayout.Toolbar(newStateType, stateTypes);
            GUILayout.EndHorizontal();
            GUILayout.EndVertical();
            GUILayout.EndArea();

            GUILayout.BeginArea(new Rect(2 * contentPadding, windowHeight * 0.25f, windowWidth - 4 * contentPadding, 20));
            GUILayout.BeginVertical();
            GUILayout.BeginHorizontal();
            GUILayout.Label("New state name: ");
            GUILayout.FlexibleSpace();
            stateName = EditorGUILayout.TextField(stateName, GUILayout.Width(675));
            GUILayout.EndHorizontal();
            GUILayout.EndVertical();
            GUILayout.EndArea();

            string stateSufix = "State";
            string substaateSufix = "Substate";

            string systemTip = "";
            GUIStyle systemTipStyle = errorTipStyle;
            if (string.IsNullOrEmpty(stateName))
            {
                systemTip = "State name can't be empty!";
            }
            else if (!stateName.EndsWith(stateSufix) && !stateName.EndsWith(substaateSufix))
            {
                systemTipStyle = infoTipStyle;
                string currentSuffix = stateName.EndsWith(stateSufix) || newStateType == 0 ? stateSufix : substaateSufix;
                systemTip = "State name ok. Note: Will autoadd (" + currentSuffix + ") sufix";
            }
            else
            {
                systemTipStyle = infoTipStyle;
                systemTip = "System name ok!";
            }

            GUILayout.BeginArea(new Rect(125.0f, windowHeight * 0.28f, 200, 20));
            EditorGUILayout.LabelField(systemTip, systemTipStyle);
            GUILayout.EndArea();

            float createButtonWidth = windowWidth * 0.8f;
            GUILayout.BeginArea(new Rect(windowWidth * 0.5f - createButtonWidth * 0.5f, windowHeight * 0.305f, createButtonWidth, 30));

            GUI.enabled = ReadyToCreateState() && !IsStateAlreadyCreated(stateName);
            if (GUILayout.Button("Create"))
            {
                if (!stateName.EndsWith(stateSufix) && newStateType == 0)
                    stateName += stateSufix;
                else if (!stateName.EndsWith(substaateSufix) && newStateType == 1)
                    stateName += substaateSufix;

                string folderPath = "Assets/Scripts/Systems";
                folderPath = AssetUtils.RelativeToAbsolutePath(folderPath, PathReferenceRoot.Project, false, SlashPolicy.Auto);
                string statesNamespace = "Gameplay.Flow";
                string namespaceToPath = statesNamespace.Replace(".", "/");

                folderPath = Path.Combine(folderPath, namespaceToPath);

                //string filePath = Path.Combine(folderPath, namespaceToPath, newStateName + ".cs");

                string idType = (StateNodeType)newStateType == StateNodeType.State ? "AppStateName" : "AppSubstateName";
                StatesManagementExtension.UpdateStatesNames(idType, stateName);

                MonoScript newStateCreated = StateUtils.CreateNewState((StateNodeType)newStateType, stateName, systemNamespaceId, folderPath);

                if (newStateCreated != null)
                {
                    GetEvent<StateNodeType, string>(StateManagementEvent.RestoreMissingStateObject).Report((StateNodeType)newStateType, stateName);

                    int result = EditorUtility.DisplayDialogComplex("State creation: success", "System " + stateName + " created!", "Another one", "Close", "Open system");

                    if (result == 0) //another one
                    {
                        stateName = "";
                    }
                    else if (result == 1) //just close
                    {
                        CloseWindow();
                    }
                    else if (result == 2) //close & openscript 
                    {
                        UnityEngine.Debug.Log("Opening state file...");
                        CloseWindow();
                        string systemFilePath = StateUtils.CreateScriptFullPath(folderPath, systemNamespaceId, stateName);
                        System.Diagnostics.Process.Start(systemFilePath);
                    }
                }
            }

            GUI.enabled = true;
            GUILayout.EndArea();

            GUILayout.BeginArea(new Rect(contentPadding * 2, windowHeight * 0.945f, windowWidth - 4 * contentPadding, 30));
            if (GUILayout.Button("Close"))
            {
                CloseWindow();
            }
            GUILayout.EndArea();

            GUILayout.BeginArea(new Rect(borderPadding + windowWidth / 2.0f - 90.0f, windowHeight * 0.345f, 180, 20));
            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            GUILayout.Label("Existing States");
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();
            GUILayout.EndArea();
        }

        /// <summary>
        /// Returns the Script of selected System
        /// </summary>
        /// <param name="targetType">Type of the script we want to get</param>
        /// <returns>MonoScript instance of the target type</returns>
        public static MonoScript GetSystemScript(Type targetType)
        {
            MonoScript[] allMonoScripts = MonoImporter.GetAllRuntimeMonoScripts();

            for (int i = 0; i < allMonoScripts.Length; i++)
            {
                Type scriptType = allMonoScripts[i].GetClass();
                if (scriptType != null && scriptType == targetType)
                {
                    return allMonoScripts[i];
                }
            }

            return null;
        }

        /// <summary>
        /// Checks if creation of the Substate/State is possible
        /// </summary>
        /// <returns>Returns ture if state name is filled, false if otherwise</returns>
        public static bool ReadyToCreateState()
        {
            return !string.IsNullOrEmpty(window.stateName);
        }

        /// <summary>
        /// Closes the window immediately
        /// </summary>
        public static void CloseWindow()
        {
            if (window != null)
            {
                window.Close();
            }
            else
            {
                StatesManagementExtensionWindow statesManagementWindow = GetWindow<StatesManagementExtensionWindow>();

                statesManagementWindow.Close();
            }
        }

        /// <summary>
        /// Checks if State of the specific name is already created
        /// </summary>
        /// <param name="stateName">Name of the State/Substate to be checked</param>
        /// <returns>Returns true if script is already created, false otherwise</returns>
        public static bool IsStateAlreadyCreated(string stateName)
        {
            MonoScript[] allMonoScripts = MonoImporter.GetAllRuntimeMonoScripts();

            for (int i = 0; i < allMonoScripts.Length; i++)
            {
                Type scriptType = allMonoScripts[i].GetClass();
                if (scriptType != null && scriptType.Name.Equals(stateName, StringComparison.OrdinalIgnoreCase))
                {
                    return true;
                }
            }
            return false;
        }
    }
}
