using Engine.Core;
using Spark.Editor;
using Spark.Framework.Components;
using Spark.Framework.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

using UnityScene = UnityEngine.SceneManagement.Scene;

namespace Extensions.StatesManagement
{
    /// <summary>
    /// Events of the States management
    /// </summary>
    public enum StateManagementEvent
    {
        /// <summary>
        /// Restoring of the state, based on reported States in the Settings
        /// </summary>
        RestoreMissingStateObject,
        /// <summary>
        /// Removal of the Substate/State
        /// </summary>
        DeleteTargetState
    }

    /// <summary>
    /// Extension script that provides the necessary logic that fulfills the management tools like creation,
    /// removal and others.
    /// </summary>
    [InitializeOnLoad]
	public class StatesManagementExtension : SparkEditorExtension<StatesManagementSettings>
	{
		private static SparkFrameworkSettings frameworkSettings = null;

        private static AppFlowStateControlSystem stateMachinePrefab = null;

        /// <summary>
        /// Initialization of the extension instance
        /// </summary>
        static StatesManagementExtension()
		{
            InitializeSettingsUpdate();

			AttachUpdate(OnUpdate);

			AttachListener<SettingsComponent, Type>(EditorExtensionEvent.SettingsFileUpdate, OnInitialize, -1);
			AttachListener(SparkProjectEvent.UncorrectSetupDetected, OnUncorrectSetupDetected, -1);
			AttachListener<SparkFrameworkSettings>(SparkProjectEvent.NewSparkSettingsDetected, OnNewSparkSettingsDetected, -1);
            AttachListener<StateNodeType, string>(StateManagementEvent.RestoreMissingStateObject, OnRestoreMissingState, -1);
            AttachListener<StateNodeType, string>(StateManagementEvent.DeleteTargetState, OnDeleteTargetState, -1);
		}

        /// <summary>
        /// Finds the precise State/Substate GmaeObject from the AppFlowStateControlSystem instance
        /// </summary>
        /// <param name="name">Name of the state we want to get the GameObject</param>
        /// <param name="systemInstance">AppFlowStateControlSystem instance</param>
        /// <returns></returns>
        public static GameObject FindStateNode(string name, AppFlowStateControlSystem systemInstance)
        {
            AppFlowState[] states = systemInstance.GetComponentsInChildren<AppFlowState>();

            for (int i = 0; i < states.Length; i++)
            {
                if (states[i].name.Equals(name, StringComparison.OrdinalIgnoreCase))
                {
                    return states[i].gameObject;
                }
            }

            AppFlowSubstate[] substates = systemInstance.GetComponentsInChildren<AppFlowSubstate>();

            for (int i = 0; i < substates.Length; i++)
            {
                if (substates[i].name.Equals(name, StringComparison.OrdinalIgnoreCase))
                {
                    return substates[i].gameObject;
                }
            }

            return null;
        }

        /// <summary>
        /// Returns scene name of the State/Substate that is already in tha AppFlowStateControlSystem prefab
        /// </summary>
        /// <param name="name">Name of the target State/Substate</param>
        /// <param name="systemInstance">Instance of hte AppFlowStateControlSystem</param>
        /// <returns></returns>
        public static string GetSceneName(string name, AppFlowStateControlSystem systemInstance)
        {
            AppFlowState[] states = systemInstance.GetComponentsInChildren<AppFlowState>();

            for (int i = 0; i < states.Length; i++)
            {
                if (states[i].name.Equals(name, StringComparison.OrdinalIgnoreCase))
                {
                    return states[i].sceneAssetReference.sceneName;
                }
            }

            AppFlowSubstate[] substates = systemInstance.GetComponentsInChildren<AppFlowSubstate>();

            for (int i = 0; i < substates.Length; i++)
            {
                if (substates[i].name.Equals(name, StringComparison.OrdinalIgnoreCase))
                {
                    return substates[i].sceneAssetReference.sceneName;
                }
            }

            return null;
        }

        /// <summary>
        /// Handles deleting target State/Substate
        /// </summary>
        /// <param name="stateNodeType">Type of State or Substate</param>
        /// <param name="stateName">Name of the Substate/State</param>
        private static void OnDeleteTargetState(StateNodeType stateNodeType, string stateName)
        {
            UnityEngine.Debug.Log("State to be destroyed: <color=green>" + stateName + "</color>");

            AppFlowStateControlSystem systemInstance = null;

            if (stateMachinePrefab != null)
            {
                systemInstance = GameObject.Instantiate(stateMachinePrefab);
            }

            GameObject stateGameObject = FindStateNode(stateName, systemInstance);

            //=================remove scene from build settings
            List<EditorBuildSettingsScene> editorScenes = EditorBuildSettings.scenes.ToList();
            string scenePath = string.Empty;

            string sceneName = GetSceneName(stateName, systemInstance);
            
            if (!string.IsNullOrEmpty(sceneName))
            {
                UnityEngine.Debug.Log("Scene name:  <color=green>" + sceneName + "</color>");

                for (int i = 0; i < editorScenes.Count; i++)
                {
                    if (editorScenes[i].path.Contains(sceneName))
                    {
                        scenePath = editorScenes[i].path;
                        UnityEngine.Debug.Log("Path of scene TBD: <color=green>" + scenePath + "</color>");
                        editorScenes.RemoveAt(i);
                        break;
                    }
                }

                EditorBuildSettings.scenes = editorScenes.ToArray();
            }
            else
            {
                UnityEngine.Debug.Log("Scene <color=red>name not found</color>");
            }

            //=================delete scene
            AssetDatabase.DeleteAsset(scenePath);

            //=================remove state child from appflow prefab
            string idName = GetStateIdName(stateGameObject);

            if (stateGameObject != null)
            {
                UnityEngine.Debug.Log("GameObject TBD: <color=green>" + stateGameObject.name + "</color>");
                GameObject.DestroyImmediate(stateGameObject);
            }

            systemInstance.appFlowStates = systemInstance.GetComponentsInChildren<AppFlowState>();
            systemInstance.substates = systemInstance.GetComponentsInChildren<AppFlowSubstate>();

            string machineStateSystemPath = AssetDatabase.GetAssetPath(stateMachinePrefab);
            PrefabUtility.SaveAsPrefabAsset(systemInstance.gameObject, machineStateSystemPath);
            GameObject.DestroyImmediate(systemInstance.gameObject);

            //=================delete script
            Dictionary<string, MonoScript> currentStates = GetStateScripts();
            MonoScript stateType = null;
            currentStates.TryGetValue(stateName, out stateType);

            string scriptPath = AssetDatabase.GetAssetPath(stateType);
            UnityEngine.Debug.Log("Path of script TBD: <color=green>" + scriptPath + "</color>");

            File.Delete(scriptPath);

            //=================remove the state enum
            RemoveStateName(idName, false);

            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }

        /// <summary>
        /// Returns the State/Substate id from the attached to the GameObject 
        /// </summary>
        /// <param name="stateObject">GameObject containing AppFlow/Substate</param>
        /// <returns>Returns the string containing the id of the State/Substate or empty string if there is no such Script attached</returns>
        private static string GetStateIdName(GameObject stateObject)
        {
            AppFlowState state = stateObject.GetComponent<AppFlowState>();
            
            if (state != null)
            {
                return state.id.ToString();
            }
            
            AppFlowSubstate subState = stateObject.GetComponent<AppFlowSubstate>();
            if (subState != null)
            {
                return subState.id.ToString();
            }

            return string.Empty;
        }

        /// <summary>
        /// Reports the missing state node to the Settings of the extension so that
        /// in the first possible occation it can be restored in the Update routine
        /// </summary>
        /// <param name="stateType">Type of the State or Substate</param>
        /// <param name="stateName">Name of the State/Substate</param>
        private static void OnRestoreMissingState(StateNodeType stateType, string stateName)
        {
            if (string.IsNullOrEmpty(stateName))
                return;

            UnityEngine.Debug.Log("Attempt to create " + stateName);

            if (stateType == StateNodeType.State)
            {
                if (!settings.requestedStates.Contains(stateName))
                {
                    settings.requestedStates.Add(stateName);
                    SaveSettings();
                }
            }
            else if (stateType == StateNodeType.Substate)
            {
                if (!settings.requestedSubstates.Contains(stateName))
                {
                    settings.requestedSubstates.Add(stateName);
                    SaveSettings();
                }
            }
        }

        /// <summary>
        /// Initialization of the StatesManagement extension, with settings
        /// </summary>
        /// <param name="settingsComponent">Settings of the extension</param>
        /// <param name="settingsType">Settings type</param>
        public static void OnInitialize(SettingsComponent settingsComponent, Type settingsType)
		{
            if (settingsType == typeof(StatesManagementSettings))
            {
                settings = (StatesManagementSettings)settingsComponent;
                initializeComplete = true;
            } 

            MonoScript stateMachineScript = GetSystemScript(typeof(AppFlowStateControlSystem));
            string machineStateSystemPath = FindAppFlowSystemPrefab(stateMachineScript);

            if (stateMachinePrefab == null)
                stateMachinePrefab = AssetDatabase.LoadAssetAtPath(machineStateSystemPath, typeof(AppFlowStateControlSystem)) as AppFlowStateControlSystem;
        }

		private static void OnNewSparkSettingsDetected(SparkFrameworkSettings newSettings)
		{
			if (frameworkSettings != null)
			{
				//include here if there were previous settings but you need to refresh anything before you switch them
			}

			frameworkSettings = newSettings;

			// include here what you want to do when new SparkSettings are detected
		}
		public static void OnUncorrectSetupDetected()
		{
			if (!EditorApplication.isPlayingOrWillChangePlaymode && !EditorApplication.isUpdating && !uncorrectSetupDetectionMarked)
			{
				uncorrectSetupDetectionMarked = true;

				//on first time we detect uncorrect setup we can do something - we add it here
			}
		}

		[MenuItem("Spark/Extensions/States Management", false, 7)]
		public static void ShowExtensionWindow()
		{
			StatesManagementExtensionWindow.Init(sparkEditorSettings);
		}

        /// <summary>
        /// Update of the Extension - responsible to finalize the reported States/Substates
        /// </summary>
		public static void OnUpdate()
		{
            if (stateMachinePrefab != null)
            {
                if (settings.requestedStates.Count > 0) 
                {
                    if (FinalizeNewState(StateNodeType.State, settings.requestedStates[0]))
                    {
                        UnityEngine.Debug.Log("State created " + settings.requestedStates[0]);
                        settings.requestedStates.RemoveAt(0);
                        SaveSettings();
                    }
                }
                else if (settings.requestedSubstates.Count > 0)
                {
                    if (FinalizeNewState(StateNodeType.Substate, settings.requestedSubstates[0]))
                    {
                        UnityEngine.Debug.Log("Substate created " + settings.requestedSubstates[0]);
                        settings.requestedSubstates.RemoveAt(0);
                        SaveSettings();
                    }
                }
            }
		}

        /// <summary>
        /// Responsible for creation of the proper GameObject with the State/Substate script and 
        /// the scene that will be attached to the State/Substate SceneReference field
        /// </summary>
        /// <param name="nodeType">Substate or State type</param>
        /// <param name="newStateTypeName">Name of the target State/Substate</param>
        /// <returns>Returns true if finalization was successful or false if otherwise</returns>
        private static bool FinalizeNewState(StateNodeType nodeType, string newStateTypeName)
        {
            Dictionary<string, MonoScript> currentStates = GetStateScripts();

            MonoScript stateType = null;

            currentStates.TryGetValue(newStateTypeName, out stateType);

            if (stateType == null)
                return false;

            GameObject newStateGameObject = null;

            if (stateMachinePrefab != null)
            {
                AppFlowStateControlSystem stateMachineInstance = GameObject.Instantiate(stateMachinePrefab);

                newStateGameObject = new GameObject(newStateTypeName);
                newStateGameObject.transform.SetParent(stateMachineInstance.transform);

                int childCount = stateMachineInstance.transform.childCount;

                for (int i = 0; i < childCount; i++)
                {
                    stateMachineInstance.transform.GetChild(i);
                }

                Component newState = newStateGameObject.AddComponent(stateType.GetClass());

                if (newState is AppFlowState)
                {
                    AppFlowState appFlowState = (AppFlowState)newState;

                    AppStateName newStateId = AppStateName.Dummy;

                    if (Enum.TryParse(newStateTypeName, out newStateId))
                    {
                        appFlowState.id = newStateId;
                    }

                    SceneAssetReference sceneAssetRef = SaveSceneAsset(newStateTypeName, nodeType);
                    appFlowState.sceneAssetReference = sceneAssetRef;

                    UnityEngine.Debug.Log("Finalized state creation, scene path: " + appFlowState.sceneAssetReference.scenePath);

                    PrefabUtility.SavePrefabAsset(stateMachinePrefab.gameObject);
                }
                else if (newState is AppFlowSubstate)
                {
                    AppFlowSubstate appFlowSubstate = (AppFlowSubstate)newState;

                    AppSubstateName newStateId = AppSubstateName.Debug;

                    if (Enum.TryParse(newStateTypeName, out newStateId))
                    {
                        appFlowSubstate.id = newStateId;
                    }

                    SceneAssetReference sceneAssetRef = SaveSceneAsset(newStateTypeName, nodeType);
                    appFlowSubstate.sceneAssetReference = sceneAssetRef;

                    UnityEngine.Debug.Log("Finalized state creation, scene path: " + appFlowSubstate.sceneAssetReference.scenePath);

                    PrefabUtility.SavePrefabAsset(stateMachinePrefab.gameObject);
                }

                stateMachineInstance.appFlowStates = stateMachineInstance.GetComponentsInChildren<AppFlowState>();
                stateMachineInstance.substates = stateMachineInstance.GetComponentsInChildren<AppFlowSubstate>();

                string machineStateSystemPath = AssetDatabase.GetAssetPath(stateMachinePrefab);
                PrefabUtility.SaveAsPrefabAsset(stateMachineInstance.gameObject, machineStateSystemPath);
                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();

                GameObject.DestroyImmediate(stateMachineInstance.gameObject);
            }

            return true;
        }

        /// <summary>
        /// Saves the scene asset in the proper localization 
        /// </summary>
        /// <param name="newStateTypeName">The state type name to be created and saved</param>
        /// <param name="nodeType">The type State or Substate</param>
        /// <returns></returns>
        private static SceneAssetReference SaveSceneAsset(string newStateTypeName, StateNodeType nodeType)
        {
            string scenePath = string.Empty;
            UnityScene stateScene = GetNewScene(newStateTypeName, nodeType, out scenePath);

            List<EditorBuildSettingsScene> editorScenes = EditorBuildSettings.scenes.ToList();

            EditorBuildSettingsScene newStateSceneEntry = null;
            TryGetSceneEntryFromEditorSettings(editorScenes, newStateTypeName, out newStateSceneEntry);
            newStateSceneEntry.path = scenePath;
            newStateSceneEntry.enabled = true;

            string fullScenePath = Path.Combine(Application.dataPath, scenePath);
            string metaFilePath = fullScenePath + ".meta";
            string[] metaLines = File.ReadAllLines(metaFilePath);
            string guid = string.Empty;

            for (int i = 0; i < metaLines.Length; i++)
            {
                if (metaLines[i].Contains("guid:"))
                {
                    guid = metaLines[i].Split(':')[1];
                    break;
                }
            }

            UnityEditor.GUID sceneGuid;
            if (UnityEditor.GUID.TryParse(guid.Trim(), out sceneGuid))
            {
                newStateSceneEntry.guid = sceneGuid;
            }

            EditorBuildSettings.scenes = editorScenes.ToArray();

            SceneAssetReference sceneAssetReference = new SceneAssetReference(stateScene);
            sceneAssetReference.scenePath = Path.Combine("Assets", scenePath);
            sceneAssetReference.sceneName = Path.GetFileNameWithoutExtension(scenePath);
            sceneAssetReference.sceneIndex = SceneUtility.GetBuildIndexByScenePath(fullScenePath);

            return sceneAssetReference;
        }

        /// <summary>
        /// Retrievies the EditorBuildSettingsscene from the EditorBuilodSettingsScene list
        /// </summary>
        /// <param name="scenes">List of the scenes from the Editor settings</param>
        /// <param name="newStateSceneName">Name of the target scene</param>
        /// <param name="entry">Result entry</param>
        /// <returns></returns>
        public static bool TryGetSceneEntryFromEditorSettings(List<EditorBuildSettingsScene> scenes, string newStateSceneName, out EditorBuildSettingsScene entry)
        {
            for (int i = 0; i < scenes.Count; i++)
            {
                string name = Path.GetFileNameWithoutExtension(scenes[i].path);
                if (name.Equals(newStateSceneName))
                {
                    entry = scenes[i];
                    return true;
                }
            }

            entry = new EditorBuildSettingsScene();
            scenes.Add(entry);
            return false;
        }

        /// <summary>
        /// Creates scene with the proper default setup according to Unity settings based on the State/Substate name
        /// </summary>
        /// <param name="stateName">Name of the State/Substate</param>
        /// <param name="type">Substate/State type</param>
        /// <param name="scenePath">Path to the scene</param>
        /// <returns></returns>
        public static UnityScene GetNewScene(string stateName, StateNodeType type, out string scenePath)
        {
            UnityScene newScene = EditorSceneManager.NewScene(NewSceneSetup.DefaultGameObjects, NewSceneMode.Additive);

            newScene.name = stateName;

            string sceneLocation = Path.Combine(Application.dataPath, "Scenes", type.ToString() + 's');

            if (!Directory.Exists(sceneLocation))
            {
                Directory.CreateDirectory(sceneLocation);
            }

            scenePath = Path.Combine(sceneLocation, stateName + ".unity");

            EditorSceneManager.SaveScene(newScene, scenePath);

            scenePath = scenePath.Remove(0, Application.dataPath.Length + 1);

            EditorSceneManager.CloseScene(newScene, true);

            AssetDatabase.Refresh();

            return newScene;
        }

        /// <summary>
        /// Returns dictionary of MonoScript based on the names of the State/Substate type
        /// </summary>
        /// <returns>Dictionary of the MonoScripts containing only State/Substate scripts</returns>
        private static Dictionary<string, MonoScript> GetStateScripts()
        {
            MonoScript[] allMonoScripts = MonoImporter.GetAllRuntimeMonoScripts();

            Dictionary<string, MonoScript> states = new Dictionary<string, MonoScript>();

            for (int i = 0; i < allMonoScripts.Length; i++)
            {
                Type scriptType = allMonoScripts[i].GetClass();
                if (scriptType != null && (scriptType.IsSubclassOf(typeof(AppFlowState)) || scriptType.IsSubclassOf(typeof(AppFlowSubstate))))
                {
                    states.Add(scriptType.Name, allMonoScripts[i]);
                }
            }

            return states;
        }

        /// <summary>
        /// Returns MonoScript with the target Type
        /// </summary>
        /// <param name="targetType">Type of the MonoScript we want to get</param>
        /// <returns>MonoScript containing the target Type</returns>
        private static MonoScript GetSystemScript(Type targetType)
        {
            MonoScript[] allMonoScripts = MonoImporter.GetAllRuntimeMonoScripts();

            for (int i = 0; i < allMonoScripts.Length; i++)
            {
                Type scriptType = allMonoScripts[i].GetClass();
                if (scriptType != null && scriptType == targetType)
                {
                    return allMonoScripts[i];
                }
            }

            return null;
        }

        /// <summary>
        /// Finds the AppFlowStateControlSystem prefab based on the MonoScript
        /// </summary>
        /// <param name="script">MonoScript containing the target AppFlowStateControlSystem</param>
        /// <returns>MonoScript with the system</returns>
        private static string FindAppFlowSystemPrefab(MonoScript script)
        {
            if (script == null)
            {
                Debug.LogError("Please select a script to find.");
                return null;
            }

            string scriptPath = AssetDatabase.GetAssetPath(script);
            string[] guids = AssetDatabase.FindAssets("t:Prefab");

            foreach (string guid in guids)
            {
                string prefabPath = AssetDatabase.GUIDToAssetPath(guid);
                GameObject prefab = AssetDatabase.LoadAssetAtPath<GameObject>(prefabPath);

                if (prefab != null)
                {
                    // Check if the prefab has the specified script attached
                    MonoBehaviour[] components = prefab.GetComponentsInChildren<MonoBehaviour>(true);
                    if (Array.Exists(components, component => component != null && component.GetType().ToString().Equals(script.GetClass().ToString(), StringComparison.OrdinalIgnoreCase)))
                    {
                        return prefabPath;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Updates the list of identifiers with the States/Substates in the AppFlowStateControlSystem
        /// </summary>
        /// <param name="nodeType">Type of the State/Substate to be added</param>
        /// <param name="stateName">Name of the Substate/State to be added</param>
        /// <param name="forceRefresh">Should it refresh the assets immediately or not</param>
        public static void UpdateStatesNames(string nodeType, string stateName, bool forceRefresh = false)
        {
            MonoScript appStateControlScript = GetSystemScript(typeof(AppFlowStateControlSystem));

            if (appStateControlScript != null)
            {
                string systemPath = AssetDatabase.GetAssetPath(appStateControlScript);

                string scriptText = appStateControlScript.text;

                string[] lines = scriptText.Split('\n');

                List<string> newScriptLines = new List<string>();

                bool enumFound = false;
                bool stateAdded = false;
                for (int i = 0; i < lines.Length; i++)
                {
                    if (!stateAdded)
                    {
                        if (lines[i].Contains(nodeType))
                        {
                            enumFound = true;
                            UnityEngine.Debug.LogWarning("Found AppStateName enum");
                        }

                        if (enumFound && lines[i - 1].Contains(',') && lines[i].Contains('}'))
                        {
                            UnityEngine.Debug.LogWarning("Found enum brackets");
                            stateAdded = true;
                            newScriptLines.Add("\t\t" + stateName + ',');
                        }
                    }
                    
                    string clearLine = lines[i].TrimEnd('\r');
                    newScriptLines.Add(clearLine);
                }

                if (File.Exists(systemPath))
                {
                    File.Delete(systemPath);
                }
                
                File.WriteAllLines(systemPath, newScriptLines.ToArray());

                if (forceRefresh)
                    AssetDatabase.Refresh();
            }
        }

        /// <summary>
        /// Removes the state id from the AppFlowStateControlSystem enums
        /// </summary>
        /// <param name="idName">Name to be removed</param>
        /// <param name="forceRefresh">Should it refresh the assets immediately or not</param>
        public static void RemoveStateName(string idName, bool forceRefresh = false)
        {
            MonoScript appStateControlScript = GetSystemScript(typeof(AppFlowStateControlSystem));

            if (appStateControlScript != null)
            {
                string systemPath = AssetDatabase.GetAssetPath(appStateControlScript);

                string scriptText = appStateControlScript.text;

                string[] lines = scriptText.Split('\n');

                List<string> newScriptLines = new List<string>(lines);

                bool enumFound = false;
                for (int i = 0; i < lines.Length; i++)
                {
                    if (!enumFound && lines[i].Contains("AppStateName"))
                    {
                        enumFound = true;
                    }

                    if (enumFound && lines[i - 1].Contains(',') && lines[i].Contains(idName))
                    {
                        UnityEngine.Debug.Log("Found line with id: <color=green>" + idName + "</color>");
                        newScriptLines.RemoveAt(i);
                        break;
                    }
                }

                for (int i = 0;i < newScriptLines.Count; i++)
                {
                    newScriptLines[i] = newScriptLines[i].TrimEnd('\r');
                }

                if (File.Exists(systemPath))
                {
                    File.Delete(systemPath);
                }

                int lastBracket = newScriptLines.FindLastIndex(s => s.Contains("}"));
                if (lastBracket + 1 < newScriptLines.Count)
                {
                    newScriptLines = newScriptLines.GetRange(0, lastBracket + 1);
                }
                
                File.WriteAllLines(systemPath, newScriptLines.ToArray());
                
                if (forceRefresh)
                    AssetDatabase.Refresh();
            }
        }
    }
}
