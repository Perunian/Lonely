using Engine.Core;
using Extensions.StatesManagement;
using Spark.Editor;
using UnityEditor;
using UnityEngine;

public enum StateNodeType
{
    State,
    Substate
}

[CustomEditor(typeof(AppFlowStateControlSystem))]
public class AppFlowStateControlSystemInspector : Editor
{
    public string newStateName = "ExampleNew";

    /// <summary>
    /// Overrides the current AppFlowStateControlSystem inspector to add convenience button that
    /// spawns the StateManagementExtenionWindow and allows to add new State/Substate
    /// </summary>
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        EditorGUILayout.BeginVertical();

        if (GUILayout.Button("Add state"))
        {
            SparkEditorSettings sparkEditorSettings = Spark.Editor.Utils.ComponentUtils.GetEditorSettings<SparkEditorSettings>(SparkEditorSettings.defaultSparkEditorSettingsName);

            StatesManagementExtensionWindow.Init(sparkEditorSettings);
        }

        EditorGUILayout.EndVertical();
    }
}