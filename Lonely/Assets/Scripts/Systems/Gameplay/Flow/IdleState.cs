using Engine.Core;
using System.Collections;
using UnityEngine;

/// <summary>
/// Idles tate that performs almost nothing, contains delayed Enter and Exit implementations
/// to show how this "dummy" state can serve as delayed hollow state for setup of the process
/// </summary>
public class IdleState : AppFlowState
{
    public float enterSecondsCooldown = 0;
    public float exitSecondsCooldown = 0;

    /// <summary>
    /// Hollow CanEnter implementation
    /// </summary>
    /// <returns></returns>
    public override bool CanEnter()
    {
        return true;
    }

    /// <summary>
    /// Hollow CanExit implementation
    /// </summary>
    /// <returns></returns>
    public override bool CanExit()
    {
        return true;
    }

    /// <summary>
    /// Delayed Enter implementation coroutine
    /// </summary>
    /// <returns></returns>
    public override IEnumerator Enter()
    {
        while (enterSecondsCooldown > 0)
        {
            enterSecondsCooldown -= Time.deltaTime;
            UnityEngine.Debug.Log("Entering in: " + enterSecondsCooldown);
            enterComplete = false;
            yield return null;
        }

        UnityEngine.Debug.Log("Should enter " + id);
        enterComplete = true;
    }

    /// <summary>
    /// Delayed Exit implementation coroutine
    /// </summary>
    /// <returns></returns>
    public override IEnumerator Exit()
    {
        while (exitSecondsCooldown > 0)
        {
            exitSecondsCooldown -= Time.deltaTime;
            exitComplete = false;
            yield return null;
        }

        UnityEngine.Debug.Log("Should exit " + id);
        exitComplete = true;
    }

    /// <summary>
    /// Empty idle
    /// </summary>
    public override void Idle()
    {

    }
}