using Spark.Framework.Components;
using Engine.Core;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using Gameplay.UI;
using DG.Tweening;

namespace Gameplay.Flow
{
	public class FadeInOutSubstate : AppFlowSubstate
	{
		public FadeInOutUiComponent fadeInOutUi = null;

		/// <summary>
		/// CanEnter routine hollow implementation
		/// </summary>
		/// <returns></returns>
		public override bool CanEnter()
		{
			return true;
		}

		/// <summary>
		/// Enter routine assuming we have prepared FadeInOutSubstate scene with proper GUI components
		/// </summary>
		/// <returns></returns>
		public override IEnumerator Enter()
		{
			GetEvent<AppSubstateName>(AppStateControlEvent.SetSubstate).Report(AppSubstateName.Debug);

			AsyncOperation loadingthread = SceneManager.LoadSceneAsync(sceneAssetReference.sceneIndex, LoadSceneMode.Additive);

			while (!loadingthread.isDone)
			{
				enterComplete = false;
				yield return null;
			}

			fadeInOutUi = scene.GetUniq<FadeInOutUiComponent>(UiStateId.FadeInOut);

			if (fadeInOutUi != null)
			{
				fadeInOutUi.couratin.unityImage.color = new Color(0, 0, 0, 0);
				fadeInOutUi.couratin.unityImage.DOFade(1, 0.5f).OnComplete(OnFadeInComplete);
			}

			enterComplete = true;
		}

		/// <summary>
		/// Callback that reports fade in completition
		/// </summary>
		public void OnFadeInComplete()
		{
			GetEvent(AppStateControlEvent.FadeInComplete).Report();
		}

		/// <summary>
		/// FadeOutComplete routine that reports the fade out completition
		/// </summary>
		/// <returns></returns>
		public IEnumerator OnFadeOutComplete()
		{
            AsyncOperation unloadingThread = SceneManager.UnloadSceneAsync(sceneAssetReference.sceneIndex);

            while (!unloadingThread.isDone)
            {
                exitComplete = false;
                yield return null;
            }

            GetEvent(AppStateControlEvent.FadeOutComplete).Report();

            exitComplete = true;
        }

		/// <summary>
		/// Can extit hollow implementation, no specific requirements 
		/// </summary>
		/// <returns></returns>
		public override bool CanExit()
		{
			return true;
		}

		/// <summary>
		/// Exit routine assuming we have proper scene with FadeInOuitSubstate GUI, routine hides the specific courtain 
		/// object which fades out to full transparency
		/// </summary>
		/// <returns></returns>
		public override IEnumerator Exit()
		{
            if (fadeInOutUi != null)
            {
                fadeInOutUi.couratin.unityImage.color = new Color(0, 0, 0, 1);
                fadeInOutUi.couratin.unityImage.DOFade(0, 0.5f).OnComplete(() => { StartCoroutine("OnFadeOutComplete"); });
            }

			yield return null;
		}

		/// <summary>
		/// Hollow idle as this state should not in the default implementation do anything
		/// </summary>
		public override void Idle()
		{

		}
	}
}
