using Engine.UI;
using Spark.Framework.Components;
using System;
using UnityEngine;

namespace Gameplay.UI
{
    public enum UiStateId
    {
        FadeInOut,
    }

    /// <summary>
    /// GUI structure of the FadeInOutSubstate gathering all necessary configurable GUI elements
    /// </summary>
    public class FadeInOutUiComponent : ExceptionalComponent
	{
		[SerializeField]
        private UiStateId uiStateId;

        public override Enum Id => uiStateId;

        public ImageUiComponent couratin;
		public override void OnCreate()
		{
            uiStateId = UiStateId.FadeInOut;
        }
    }
}
