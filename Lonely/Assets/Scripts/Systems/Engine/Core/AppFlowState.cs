using Spark.Framework.Events;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Engine.Core
{
    /// <summary>
    /// Template class for implementation of any necessary application state, if we want to use some state
    /// it has to inherit from this template and implement all of the necessary callbacks, also we have to put
    /// a game object containing this implementation under 
    /// </summary>
    public abstract class AppFlowState : EventComponent
    {
        //the name of the state, any new state needs to be added to the enumeration ofc
        public AppStateName id;
        //current phase of the state
        public StatePhase phase = StatePhase.None;

        public bool independent = true;

        public bool exitComplete = false;
        public bool enterComplete = false;

        public SceneAssetReference sceneAssetReference;

        /// <summary>
        /// Check for entering the state logic
        /// </summary>
        /// <returns>True if logic can be executed, false if app state is still not ready</returns>
        public abstract bool CanEnter();

        /// <summary>
        /// Handle for logic on the moment the state should be entered
        /// </summary>
        /// <returns>True if the logic was executed correctly, false otherwise</returns>
        public abstract IEnumerator Enter();

        /// <summary>
        /// Idle (executed every Update) logic during this specific state
        /// </summary>
        /// <returns>True if the logic is being executed correctly, false otherwie</returns>
        public abstract void Idle();

        /// <summary>
        /// Check for if the state can be safely exited
        /// </summary>
        /// <returns>True if the state can be exited, false if the state has still some uncorrect setup to exit</returns>
        public abstract bool CanExit();

        /// <summary>
        /// Logic for when state should exit
        /// </summary>
        /// <returns>True if logic was executed correctly, false otherwise</returns>
        public abstract IEnumerator Exit();


        public bool ExitComplete => exitComplete;

        public bool EnterComplete => enterComplete;
    }
}

