using Spark.Framework.Systems;
using System;
using System.Collections.Generic;

namespace Engine.Core
{
    /// <summary>
    /// App states identifiers, if we want to add a state, it has to be added here too
    /// </summary>
    public enum AppStateName
    {
        Dummy,
        Idle,
		AppLoadingState,
		MainMenuState,
		GameplayCharCreatorState,
		GameplayPublisherOfficeState,
		PublishersRankingState,
    }

    public enum AppSubstateName
    {
        None,
        Debug,
		ResearchStartDialogueSubstate,
		GameMainOptionsSubstate,
		FadeInOutSubstate,
    }

    /// <summary>
    /// State lifecycles identifiers
    /// </summary>
    public enum StatePhase
    {
        None,
        WaitingToEnter,
        Entering,
        Idle,
        WaitingToExit,
        Exiting,
    }

    /// <summary>
    /// Application control events identifiers
    /// </summary>
    public enum AppStateControlEvent
    {
        SetState,
        StateEntered,
        StateExited,

        SetSubstate,
        RemoveSubstate,
        SubstateEntered,
        SubstateExited,

        FadeInComplete,
        FadeOutComplete
    }

    /// <summary>
    /// Application state machine logic
    /// </summary>
    public class AppFlowStateControlSystem : BasicSystem
    {
        //available application states collection
        public AppFlowState[] appFlowStates;
        //index of the current state, -1 if there is no current state (which during runtime should be impossible)
        public int currentStateId = -1;
        //index of the next state, -1 if there is no next state to be entered
        public int nextStateId = -1;

        public AppFlowSubstate[] substates;

        private HashSet<AppSubstateName> currentSubstatesIds = new HashSet<AppSubstateName>();
        private List<AppFlowSubstate> currentSubstates = new List<AppFlowSubstate>();

        public bool waitingForFadeComplete = false;

        /// <summary>
        /// Inherited from BasicSystem, setups the "dummy" state for application to be changed
        /// </summary>
        public override void OnCreate()
        {
            UpdateAppFlowStates();

            for (int i = 0; i < appFlowStates.Length; i++)
            {
                if (appFlowStates[i].id == AppStateName.Idle)
                {
                    currentStateId = i;
                }
            }

            AppFlowState currentState = appFlowStates[currentStateId];
            if (currentState.CanEnter())
            {
                currentState.phase = StatePhase.Idle;
            }
            else
            {
                UnityEngine.Debug.LogError("SOMETHING IS WRONG, CANT ENTER IDLE STATE");
            }
        }

        /// <summary>
        /// Inherited from BasicSystem
        /// </summary>
        public override void OnInitialize()
        {
            AttachListener<AppStateName, bool>(AppStateControlEvent.SetState, OnSetState, 0);
            AttachListener<AppSubstateName>(AppStateControlEvent.SetSubstate, OnSetSubstate, 0);
            AttachListener<AppSubstateName>(AppStateControlEvent.RemoveSubstate, OnRemoveSubstate, 0);

            AttachListener(AppStateControlEvent.FadeInComplete, OnFadeInComplete, 0);
            AttachListener(AppStateControlEvent.FadeOutComplete, OnFadeOutComplete, 0);
        }

        private void OnRemoveSubstate(AppSubstateName subStateToRemove)
        {
            if (currentSubstatesIds.Contains(subStateToRemove))
            {
                for (int i = 0;  i < currentSubstatesIds.Count; i++)
                {
                    if (currentSubstates[i].id == subStateToRemove)
                    {
                        currentSubstates[i].phase = StatePhase.WaitingToExit; break;
                    }
                }
            }
            else
            {
                UnityEngine.Debug.LogWarning($"Something is wrong cause you want to remove {subStateToRemove} but it's not registered!");
            }
        }

        private void OnFadeOutComplete()
        {
            if (waitingForFadeComplete)
            {
                waitingForFadeComplete = false;
            }
        }

        private void OnFadeInComplete()
        {
            if (waitingForFadeComplete)
            {
                InvokeRoadToNewState();
            }
        }

        /// <summary>
        /// Inherited from BasicSystem, creates the app config to be sure everything runs smooth from this point
        /// </summary>
        public override void OnStart()
        {
            //AppConfigData appConfig = New<AppConfigData>();
        }

        /// <summary>
        /// Logic for setting new state of the application, if the state is different from the current
        /// </summary>
        /// <param name="newState">Identifier of the state to be set</param>
        private void OnSetState(AppStateName newState, bool useFadeOut)
        {
            if (!waitingForFadeComplete)
            {
                int newStateId = -1;
                for (int i = 0; i < appFlowStates.Length; i++)
                {
                    if (appFlowStates[i].id == newState)
                    {
                        newStateId = i;
                        break;
                    }
                }

                if (newStateId != currentStateId)
                {
                    nextStateId = newStateId;

                    if (useFadeOut)
                    {
                        OnSetSubstate(AppSubstateName.FadeInOutSubstate);
                        waitingForFadeComplete = true;
                    }
                    else
                    {
                        InvokeRoadToNewState();
                    }
                }
            }
            else
            {
                UnityEngine.Debug.LogError($"You are trying to set {newState} but fadeInOut is in progress!");
            }
        }

        public void InvokeRoadToNewState()
        {
            if (nextStateId > -1)
            {
                AppFlowState nextState = appFlowStates[nextStateId];
                nextState.phase = StatePhase.WaitingToEnter;

                if (currentStateId > -1)
                {
                    AppFlowState currentState = appFlowStates[currentStateId];

                    if (currentState.phase == StatePhase.Idle)
                    {
                        currentState.phase = StatePhase.WaitingToExit;
                    }
                }
            }
        }

        private void OnSetSubstate(AppSubstateName newSubState)
        {
            if (!currentSubstatesIds.Contains(newSubState))
            {
                AppFlowSubstate substateToAdd = null;

                for (int i = 0; i < substates.Length; i++)
                {
                    if (substates[i].id == newSubState)
                    {
                        substateToAdd = substates[i];
                        break;
                    }
                }

                if (substateToAdd?.phase == StatePhase.None)
                {
                    substateToAdd.phase = StatePhase.WaitingToEnter;

                    currentSubstates.Add(substateToAdd);
                    currentSubstatesIds.Add(newSubState);
                }
            }
        }

        /// <summary>
        /// If we have a new state to be entered we try to change it depending on specific handlers implemented in the state logic
        /// </summary>
        public override void OnUpdate()
        {
            StateUpdate();

            SubstateUpdate();
        }

        private void SubstateUpdate()
        {
            for (int i = 0; i < currentSubstates.Count; i++)
            {
                AppFlowSubstate nextSubstate = currentSubstates[i];

                if (nextSubstate.phase == StatePhase.Idle)
                {
                    nextSubstate.Idle();
                    continue;
                }

                if (nextSubstate.phase == StatePhase.WaitingToExit && nextSubstate.CanExit())
                {
                    nextSubstate.phase = StatePhase.Exiting;
                    StartCoroutine(nextSubstate.Exit());
                    UnityEngine.Debug.LogWarning("Substate exiting start: " + nextSubstate.id);
                }

                if (nextSubstate.phase == StatePhase.Exiting && nextSubstate.ExitComplete)
                {
                    GetEvent<AppSubstateName>(AppStateControlEvent.SubstateExited).Report(nextSubstate.id);
                    UnityEngine.Debug.LogWarning("Substate exited: " + nextSubstate.id);
                    currentSubstatesIds.Remove(nextSubstate.id);
                    nextSubstate.phase = StatePhase.None;
                }

                if (nextSubstate.phase == StatePhase.WaitingToEnter)
                {
                    if (nextSubstate.CanEnter())
                    {
                        nextSubstate.phase = StatePhase.Entering;
                        StartCoroutine(nextSubstate.Enter());
                    }
                }

                if (nextSubstate.phase == StatePhase.Entering && nextSubstate.EnterComplete)
                {
                    GetEvent<AppSubstateName>(AppStateControlEvent.SubstateEntered).Report(nextSubstate.id);
                    UnityEngine.Debug.LogWarning("Substate entered: " + nextSubstate.id);
                    nextSubstate.phase = StatePhase.Idle;
                }
            }
        }

        public void StateUpdate()
        {
            if (currentStateId > -1)
            {
                AppFlowState currentState = appFlowStates[currentStateId];

                if (nextStateId > -1)
                {
                    AppFlowState nextState = appFlowStates[nextStateId];

                    if (currentState.phase == StatePhase.WaitingToExit && currentState.CanExit())
                    {
                        currentState.phase = StatePhase.Exiting;
                        StartCoroutine(currentState.Exit());
                        UnityEngine.Debug.LogWarning("State exiting start: " + currentState.id);
                    }

                    if (currentState.phase == StatePhase.Exiting && currentState.ExitComplete)
                    {
                        GetEvent<AppStateName>(AppStateControlEvent.StateExited).Report(currentState.id);
                        UnityEngine.Debug.LogWarning("State exited: " + currentState.id);
                        currentState.phase = StatePhase.None;
                    }

                    if (currentState.phase == StatePhase.None)
                    {
                        if (nextState.phase == StatePhase.WaitingToEnter)
                        {
                            if (nextState.CanEnter())
                            {
                                nextState.phase = StatePhase.Entering;
                                StartCoroutine(nextState.Enter());
                            }
                        }

                        if (nextState.phase == StatePhase.Entering && nextState.EnterComplete)
                        {
                            GetEvent<AppStateName>(AppStateControlEvent.StateEntered).Report(nextState.id);
                            if (waitingForFadeComplete)
                            {
                                OnRemoveSubstate(AppSubstateName.FadeInOutSubstate);
                            }
                            UnityEngine.Debug.LogWarning("State entered: " + nextState.id);
                            nextState.phase = StatePhase.Idle;

                            if (currentState.phase == StatePhase.None)
                            {
                                currentStateId = nextStateId;
                                nextStateId = -1;
                            }
                        }
                    }
                }

                if (currentStateId > -1 && currentState.phase == StatePhase.Idle)
                {
                    currentState.Idle();
                }
            }
        }

        /// <summary>
        /// Updating available states for the application
        /// </summary>
        public void UpdateAppFlowStates()
        {
            if (appFlowStates == null || (appFlowStates != null && appFlowStates.Length == 0))
            {
                appFlowStates = GetComponentsInChildren<AppFlowState>();
            }
        }
    }
}







