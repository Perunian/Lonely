using Spark.Framework.Components;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Engine.UI
{
	public class ImageUiComponent : ObjectComponent
	{
		public RawImage unityImage;
		public override void OnCreate()
		{
			if (unityImage == null)
				unityImage = GetComponent<RawImage>();
		}

	}
}
